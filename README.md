# Zack's weblog generator

Use this program to transform Markdown content and posts into a static web
site. It's roughly analogous to Jekyll or Eleventy, but with less features.

I don't recommend using this tool for your own website.

## How to use

`zwg` is convention-based, there's minimal configuration.
To use it, **create a directory with the following structure**:

* `index.md` - root page template, written in Markdown and Liquid
* `layout.html.liquid` - layout template, written in HTML and Liquid
* `posts/*.md` - Markdown posts, written in Markdown and Liquid
* `static/`, `robots.txt` - static files, copied without any transformation

Running the `zwg <directory>` will read the contents of `<directory>` and
generate a static website in `<directory>/_public`, the contents of which
can be served by any web server, e.g.:

`$ static-web-server -d _public -p 8080`

### Template config values

All Markdown templates can include a YAML document at the top, delimited with
three dashes (`---`). Relevant keys are: `title`, `summary`, `tags` and
`illustration`/`caption`.

### Rendered template globals

The `posts` global is a collection of objects with the following schema: `url`,
`date`, `info.title`, `info.summary`, `info.tags` and `info.illustration`. The
`content` value contains the non-escaped HTML content for the page. These
values are also defined globally on the relevant page.

### Drafts

Files in `posts/` may start with a date, like `2023-11-14-my-post.md`. Posts
with no date in the name OR with the `draft` tag are left out of the `posts`
index, but are still rendered.

## TODO

### Needed for `zackhobson.com`

* [DONE] render layout template
* [DONE] copy static files
* [DONE] show correct date stamp for posts
* [DONE] separate layout for posts
* [DONE] support for other files types e.g. `atom/xml`
* [DONE] file glob patterns in page templates list
* [DONE] Eleventy-compatible "/index.html" output for pages
* [DONE] backward-compatible post URLs

### Nice to have, or Eleventy catch-up

* support for partials in Liquid templates
* configure via command line/environment
* web server for local dev
* live preview in local dev

