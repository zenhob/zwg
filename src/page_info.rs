//! Handle page metadata

use serde::{Serialize, Deserialize};
use std::collections::HashSet;
use std::path::{PathBuf, Path};

/// Page info extracted from the YAML header.
#[derive(Default, Debug, Serialize, Clone, Eq, PartialEq, Deserialize)]
pub struct PageMeta {
    pub title: Option<String>,
    pub summary: Option<String>,
    pub illustration: Option<String>,
    pub caption: Option<String>,
    #[serde(default)]
    tags: HashSet<String>,
    layout: Option<String>,
}

impl PageMeta {
    /// Query if the page info contains the specified tag.
    pub fn has_tag(&self, tag: &str) -> bool {
        self.tags.contains(tag)
    }

    pub fn layout_path(&self) -> Option<PathBuf> {
        if let Some(layout) = &self.layout {
            return Some(Path::new(&layout).to_path_buf())
        }
        None
    }

    /// Given raw page content, return the front matter, if any is present.
    pub fn from_content(raw_content: &str) -> Option<PageMeta> {
        let mut lines = raw_content.lines().peekable();
        if lines.peek() != Some(&"---") {
            return None;
        }

        let front_matter = lines.skip(1).take_while(|line| *line != "---");
        let yaml_str = front_matter.collect::<Vec<&str>>().join("\n");
        let result: Option<PageMeta> = serde_yaml::from_str(&yaml_str).ok();
        result
    }

}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE_RAW: &str = "---\ntitle: \"hello, world\"\n---\ncontent time\n";
    const EXAMPLE: &str = "title: yo\nsummary: hi\nillustration: uh\ncaption: eh\ntags: [foo, baz]";

    #[test]
    fn test_from_str() {
        let info: PageMeta = serde_yaml::from_str(EXAMPLE).unwrap_or_default();

        assert_eq!(info.title, Some("yo".to_string()));
        assert_eq!(info.summary, Some("hi".to_string()));
        assert_eq!(info.illustration, Some("uh".to_string()));
        assert_eq!(info.caption, Some("eh".to_string()));
        assert_eq!(info.tags, ["foo".to_string(), "baz".to_string()].into());
    }

    #[test]
    fn test_extract_front_matter_with_value() {
        let value = PageMeta::from_content(EXAMPLE_RAW).unwrap_or_default();
        assert_eq!(value.title, Some("hello, world".to_string()));
    }

    #[test]
    fn test_extract_front_matter_without_value() {
        assert!(PageMeta::from_content(&"whatever".to_string()).is_none());
    }

}
