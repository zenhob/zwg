
use crate::page::{Page, CollectionPost};

use anyhow::Result;
use log::info;
use std::convert::From;
use std::fs;
use std::path::{Path, PathBuf};

use crate::process;


pub struct LiquidPage {
    file_name: PathBuf,
    output_path: PathBuf,
    public_url: String,
    raw_template: String,
}

impl LiquidPage {
    pub fn from_file_path(template_path: &Path) -> Result<Self> {
        info!("reading file {}", template_path.display());

        let raw_template = fs::read_to_string(template_path)?;
        let output_path = PathBuf::from(template_path).with_extension("");

        let template = LiquidPage {
            file_name: template_path.into(),
            output_path: output_path.to_owned(),
            public_url: format!("/{}", output_path.display()),
            raw_template,
        };

        Ok(template)
    }

    fn get_globals(&self, posts: Vec<CollectionPost>) -> liquid::Object {
        // add page values to globals
        liquid::object!({
            "url": self.public_url,
            "posts": posts,
        })
    }
}

impl Page for LiquidPage {
    fn export(
        &self,
        output_dir: &str,
        _layout_path: &Path,
        posts: Vec<CollectionPost>,
    ) -> Result<PathBuf> {
        let env = self.get_globals(posts);
        let output = process::parse_liquid_template(&self.raw_template, &env)?;
        let output_path = self.prepare_output_path(&self.output_path, output_dir)?;

        info!("writing file {}", output_path.display());
        fs::write(&output_path, output)?;
        Ok(output_path)
    }

    fn file_name(&self) -> String {
        self.file_name.display().to_string()
    }
}
