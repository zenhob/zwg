//! Use this program to transform Markdown content and posts into a static web
//! site. Like Eleventy but with less features (because it's only for my website).

mod blog;
mod config;
mod date;
mod page;
mod liquid_page;
mod page_info;
mod process;

use crate::config::BlogConfig;
use anyhow::Result;
use env_logger::Env;
use log::{debug, error, info};
use std::env;
use std::path::Path;
use std::process::exit;
use std::process::ExitCode;

/// The `zwg` command. Takes no arguments.
///
/// Export posts and template files in the current directory
/// to the `_public` directory.
fn main() -> ExitCode {
    // use ZWG_LOG_LEVEL and ZWG_WRITE_STYLE to configure logging
    let env = Env::default()
        .filter("ZWG_LOG_LEVEL")
        .write_style("ZWG_WRITE_STYLE");
    env_logger::init_from_env(env);

    match handle_args() {
        Ok(_) => {},
        Err(e) => {
            error!("Failed to process arguments. {}", e);
            return ExitCode::FAILURE;
        }
    }

    let config = BlogConfig::with_zacks_defaults();
    debug!("Config: {:?}", config);

    let blog = config.build_blog();
    //debug!("Blog: {:?}", blog);

    match blog.export() {
        Ok(stats) => {
            println!(
                "Finished. Wrote {} templated files. {} failures.",
                stats.template_files_written, stats.template_files_failed
            );
            ExitCode::SUCCESS
        }
        Err(e) => {
            error!("zwg command failed. {}", e);
            ExitCode::FAILURE
        }
    }
}

fn handle_args() -> Result<()> {
    let args: Vec<String> = env::args().collect();
    if args.len() == 2 {
        info!("Using working dir {}", &args[1]);
        let working_dir = Path::new(&args[1]);
        env::set_current_dir(working_dir)?;
        Ok(())
    } else {
        usage();
        exit(1);
    }
}

fn usage() {
    println!("Usage: zwg <directory>");
}
