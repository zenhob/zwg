//! Export a single Markdown page to HTML.

use anyhow::{Context, Error, Result};
use chrono::NaiveDate;
use liquid::model::Value as LiquidValue;
use log::info;
use markdown::{CompileOptions, Options};
use serde::Serialize;
use std::cmp::Ordering;
use std::convert::From;
use std::fs;
use std::io;
use std::path::{Path, PathBuf};

use crate::date::extract_date;
use crate::page_info::PageMeta;
use crate::process;

pub trait Page {
    fn export(
        &self,
        output_dir: &str,
        layout_path: &Path,
        posts: Vec<CollectionPost>,
    ) -> Result<PathBuf>;

    fn file_name(&self) -> String;

    /// Determine the target output directory and create it if it doesn't exist.
    fn prepare_output_path(&self, output_path: &PathBuf, output_dir: &str) -> Result<PathBuf> {
        let out_path = PathBuf::from(output_dir).join(output_path);
        if let Some(parent) = out_path.parent() {
            fs::create_dir_all(parent)?;
        }
        Ok(out_path)
    }
}

/// The `posts` value in Liquid templates is a list of these.
#[derive(Default, Debug, Serialize)]
pub struct CollectionPost {
    url: String,
    info: PageMeta,
    date: String,
    content: Option<String>,
}

impl From<MarkdownPage> for CollectionPost {
    /// Convert from a Page to a CollectionPost
    fn from(page: MarkdownPage) -> Self {
        let globals = page.get_globals(vec![]);
        let content = page.as_content_str(&globals).ok();
        let date = page.liquid_date();
        Self {
            content,
            url: page.public_url,
            info: page.info,
            date,
        }
    }
}

/// Represents a single Markdown/Liquid document and website target.
/// Provides tools to convert this document into HTML.
#[derive(Default, Debug, Clone, Eq, PartialEq)]
pub struct MarkdownPage {
    file_name: PathBuf,
    public_url: String,
    output_path: PathBuf,
    info: PageMeta,
    raw_md_content: String,
    date: Option<NaiveDate>,
}

impl PartialOrd for MarkdownPage {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.date.cmp(&other.date))
    }
}
impl Ord for MarkdownPage {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.date.cmp(&other.date)
    }
}

/// The options provided to Markdown allow "dangerous" HTML and protocols,
/// since we are processing known content.
fn markdown_options() -> Options {
    Options {
        compile: CompileOptions {
            allow_dangerous_html: true,
            allow_dangerous_protocol: true,
            ..CompileOptions::default()
        },
        ..Options::default()
    }
}

// For the NaiveDate type, only the date format numbers are usable, and a bad
// format results in a run time error. So we hardcode the time as midnight.
// https://docs.rs/chrono/latest/chrono/naive/struct.NaiveDate.html#method.format

// This strftime format conforms with the input to the Liquid date filter:
// https://current-rms.gitbook.io/liquid-syntax/information/date-filter
const LIQUID_DATE_FORMAT: &str = "%Y-%m-%d 00:00:00";

// Not used, might end up part of a custom liquid filter or something for
// formatting dates for Atom.
const _ATOM_DATE_FORMAT: &str = "%Y-%m-%dT00:00:00Z";

impl MarkdownPage {
    /// Create a page given a document source path.
    pub fn from_file_path(template_path: &Path) -> Result<Self> {
        info!("reading file {}", template_path.display());

        let raw_template = fs::read_to_string(template_path)?;
        let raw_md_content = process::extract_document(&raw_template);
        let info: PageMeta = PageMeta::from_content(&raw_template).unwrap_or_default();

        let date = extract_date(&template_path.display().to_string());
        let output_path: PathBuf;
        if date.is_none() {
            output_path = MarkdownPage::generate_output_path(template_path);
        } else {
            output_path = MarkdownPage::generate_dated_path(template_path, date.unwrap());
        }
        let mut public_url = format!("/{}", output_path.display());

        if public_url.ends_with("index.html") {
            public_url = public_url.replace("index.html", "");
        }

        let template = MarkdownPage {
            file_name: template_path.into(),
            output_path: output_path.to_owned(),
            public_url,
            raw_md_content,
            info,
            date,
        };

        Ok(template)
    }

    /// If there is a layout item in the metadata, return that. Otherwise,
    /// scan parent directories for the layout file until one is located and return it.
    fn current_layout(&self, layout_path: &Path) -> Option<PathBuf> {
        // check for layout in the page YAML meta
        if let Some(meta_layout) = self.info.layout_path() {
            return Some(meta_layout);
        }

        // search for the layout file
        let mut search_dir: PathBuf = self.file_name.parent()?.into();
        loop {
            let possible_layout = search_dir.join(layout_path);
            if possible_layout.is_file() {
                break Some(possible_layout);
            }
            if !search_dir.pop() {
                break None;
            }
        }
    }

    fn liquid_date(&self) -> String {
        self.date
            .map(|d| d.format(LIQUID_DATE_FORMAT).to_string())
            .unwrap_or("today".to_string())
    }

    fn get_globals(&self, posts: Vec<CollectionPost>) -> liquid::Object {
        // add page values to globals
        liquid::object!({
            "date": self.liquid_date(),
            "url": self.public_url,
            "info": self.info.clone(),
            "posts": posts,
        })
    }

    /// Produce fully processed HTML output for export.
    fn as_html(&self, globals: &liquid::Object, layout_content: &str) -> Result<String> {
        let env = self.template_env(globals);
        let content_value = self
            .as_content_value(&env)
            .context("Failed to parse page content")?;

        // copy env with output_html added to as key `content`
        let mut layout_env = env.clone();
        layout_env.insert("content".into(), content_value);

        // render layout template with this new env
        process::parse_liquid_template(layout_content, &layout_env)
    }

    /// Returns true if the page has no date OR has the `draft` tag
    pub fn is_draft(&self) -> bool {
        self.date.is_none() || self.info.has_tag("draft")
    }

    /// Add page info to the globals for the Liquid rendering environment.
    fn template_env(&self, globals: &liquid::Object) -> liquid::Object {
        let mut page = liquid::object!(self.info);
        for (k, v) in globals.iter() {
            page.insert(k.clone(), v.clone());
        }
        page
    }

    /// Process Markdown and Liquid to create the `content` value in the layout template.
    fn as_content_str(&self, env: &liquid::Object) -> Result<String> {
        // do the liquid processing first so liquid code does not get parsed as markdown
        let md_content = process::parse_liquid_template(&self.raw_md_content, env)?;
        markdown::to_html_with_options(&md_content, &markdown_options()).map_err(Error::msg)
    }

    /// Process Markdown and Liquid to create the `content` value in the layout template.
    fn as_content_value(&self, env: &liquid::Object) -> Result<LiquidValue> {
        let html_content = self
            .as_content_str(env)
            .context("Failed to process liquid and markdown")?;
        Ok(LiquidValue::Scalar(html_content.into()))
    }

    fn generate_output_path(template_path: &Path) -> PathBuf {
        let mut output = PathBuf::from(template_path).with_extension("");
        if output.ends_with("index") {
            output = output.with_extension("html");
        } else {
            output.push("index.html");
        }
        output
    }

    fn generate_dated_path(template_path: &Path, date: NaiveDate) -> PathBuf {
        let post_name = template_path
            .display()
            .to_string()
            .split("-")
            .skip(3)
            .collect::<Vec<_>>()
            .join("-");
        let mut path = PathBuf::from(format!("{}/{}", date.format("%Y/%m/%d"), post_name)).with_extension("");
        path.push("index.html");
        path
    }
}

impl Page for MarkdownPage {
    /// Export the fully processed page to the target path on the filesystem.
    fn export(
        &self,
        output_dir: &str,
        layout_path: &Path,
        posts: Vec<CollectionPost>,
    ) -> Result<PathBuf> {
        // locate layout file
        let layout = self.current_layout(layout_path).ok_or(io::Error::new(
            io::ErrorKind::Other,
            format!("could not locate layout file: {}", layout_path.display()),
        ))?;

        let globals = self.get_globals(posts);
        let layout_content = fs::read_to_string(layout.as_path())?;
        let output_html = self.as_html(&globals, &layout_content)?;
        let output_path = self.prepare_output_path(&self.output_path, output_dir)?;

        info!("writing file {}", output_path.display());
        fs::write(&output_path, output_html)?;
        Ok(output_path)
    }

    fn file_name(&self) -> String {
        self.file_name.display().to_string()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_non_post_output_path() {
        let actual = MarkdownPage::generate_output_path(Path::new("hiworld.md"));

        let expected = Path::new("hiworld/index.html");
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_post_output_path() {
        let actual = MarkdownPage::generate_output_path(Path::new("posts/2023-11-12-hi-world.md"));

        //let expected = Path::new("2023/11/12/hi-world/index.html");
        let expected = Path::new("posts/2023-11-12-hi-world/index.html");
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_post_index_output_path() {
        let actual = MarkdownPage::generate_output_path(Path::new("index.md"));

        let expected = Path::new("index.html");
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_as_html() {
        let mut page: MarkdownPage = Default::default();
        page.raw_md_content = "# header\n\nmore stuff".to_string();
        assert_eq!(
            page.as_html(&liquid::object!({}), "<html>{{content}}</html>")
                .unwrap_or_default(),
            "<html><h1>header</h1>\n<p>more stuff</p></html>".to_string()
        );
    }

    #[test]
    fn test_from_file_fails_with_no_file() {
        assert!(!MarkdownPage::from_file_path(Path::new("nosuchfile.whatever")).is_ok());
    }

    #[test]
    fn test_liquid_assign() {
        let mut page: MarkdownPage = Default::default();
        page.raw_md_content = "\n{%- assign hello = 'world' -%}\n## {{ hello }}".to_string();
        assert_eq!(
            page.as_html(&liquid::object!({}), "<html>{{content}}</html>")
                .unwrap_or_default(),
            "<html><h2>world</h2></html>".to_string()
        );
    }

    #[test]
    fn test_html_passthru() {
        let mut page: MarkdownPage = Default::default();
        page.raw_md_content = "\n\n\n\n<h2><span>hello</span></h2>".to_string();
        assert_eq!(
            page.as_html(&liquid::object!({}), "<html>{{content}}</html>")
                .unwrap_or_default(),
            "<html><h2><span>hello</span></h2></html>".to_string()
        );
    }
}
