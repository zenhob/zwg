//! Date utilities

use chrono::NaiveDate;

/// Take a path string with a date in the middle and extract a
/// date object, if possible.
pub fn extract_date(full_path: &str) -> Option<NaiveDate> {
    let date_part: String = full_path
        .split('/')
        .filter(|s| s.len() >= 9)
        .last()?
        .split('-')
        .take(3)
        .collect::<Vec<&str>>()
        .join("-");
    NaiveDate::parse_from_str(&date_part, "%Y-%m-%d").ok()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_extract_date() {
        let input = "/posts/2023-11-15-test-post/";
        let expected = NaiveDate::from_ymd_opt(2023, 11, 15);
        assert_eq!(extract_date(input), expected);
    }

    #[test]
    fn test_extract_date_invalid_day() {
        let input = "/posts/2023-11-93-test-post/";
        assert_eq!(extract_date(input), None);
    }

    #[test]
    fn test_extract_date_invalid_month() {
        let input = "/posts/2023-14-11-test-post/";
        assert_eq!(extract_date(input), None);
    }

    #[test]
    fn test_extract_no_date() {
        let input = "/posts/test-post/";
        assert_eq!(extract_date(input), None);
    }
}
