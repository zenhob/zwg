//! Utilities for extracting and parsing liquid content

use anyhow::{Result, Context};


pub fn extract_document(raw_content: &str) -> String {
    let mut lines = raw_content.lines().peekable();
    if lines.peek() != Some(&"---") {
        return raw_content.to_string();
    }

    let body_lines = lines.skip(1).skip_while(|line| *line != "---").skip(1);
    body_lines.collect::<Vec<&str>>().join("\n")
}

pub fn parse_liquid_template(document: &str, globals: &liquid::Object) -> Result<String> {
    let template = liquid::ParserBuilder::with_stdlib()
        .build()?
        .parse(document)?;
    Ok(template.render(globals).context("error during render")?)
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "---\ntitle: \"hello, world\"\n---\ncontent time\n";

    #[test]
    fn test_extract_document_with_front_matter() {
        assert_eq!(extract_document(EXAMPLE), "content time".to_string());
    }

    #[test]
    fn test_extract_document_without_front_matter() {
        assert_eq!(extract_document("whatever"), "whatever".to_string());
    }

}
