//! Configure and build a blog

use crate::blog::Blog;
use std::path::PathBuf;

#[derive(Default, Debug)]
pub struct BlogConfig {
    layout_path: PathBuf,
    static_passthru: Vec<String>,
    pages: Vec<String>,
    posts_dir: Option<String>,
    export_target_dir: String,
}

impl BlogConfig {
    /// Instantiate an empty blog builder.
    pub fn new() -> Self {
        Default::default()
    }

    /// Prepare a builder with Zack's default settings.
    pub fn with_zacks_defaults() -> Self {
        Self::new()
            .export_dir("_public")
            .layout("layout.html.liquid")
            .posts_dir("posts")
            .add_page("*.md")
            .add_page("atom.xml.liquid")
            .add_page("resume/index.md")
            .add_static_path("static")
            .add_static_path("robots.txt")
    }

    /// Specify the liquid layout wrapper template.
    pub fn layout(mut self, layout: &str) -> Self {
        self.layout_path = layout.into();
        self
    }

    /// Specify the directory where individual blog posts are stored.
    pub fn posts_dir(mut self, dirname: &str) -> Self {
        self.posts_dir = Some(dirname.into());
        self
    }

    /// Specify the target export directory.
    pub fn export_dir(mut self, dirname: &str) -> Self {
        self.export_target_dir = dirname.into();
        self
    }

    /// Add an additional template page to the input.
    pub fn add_page(mut self, source: &str) -> Self {
        self.pages.push(source.into());
        self
    }

    /// Add a file or directory to to the list of static assets.
    pub fn add_static_path(mut self, passthru: &str) -> Self {
        self.static_passthru.push(passthru.into());
        self
    }

    /// Prepare a blog instance.
    pub fn build_blog(&self) -> Blog {
        Blog::new(
            self.pages.to_owned(),
            self.posts_dir.to_owned(),
            self.static_passthru.to_owned(),
            &self.layout_path,
            &self.export_target_dir,
        )
    }
}
