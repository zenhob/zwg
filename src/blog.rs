//! Export a web site from the current directory.

use crate::liquid_page::LiquidPage;
use crate::page::{CollectionPost, MarkdownPage, Page};
use glob::{glob_with, MatchOptions};
use anyhow::Result;
use dircpy::copy_dir;
use log::{error, info, warn};
use std::fs;
use std::io;
use std::path::{Path, PathBuf};

/// Represents a set of files and configs needed to export the web site.
#[derive(Default)]
pub struct Blog {
    /// layout file used by every exported page
    layout_path: PathBuf,
    /// directories that will be copied to the target unchanged
    static_passthru: Vec<String>,
    /// list of pages to be exported
    pages: Vec<Box<dyn Page>>,
    /// list of indexed posts to be exported
    posts: Vec<MarkdownPage>,
    /// the root of the exported web site
    export_target_dir: String,
}

#[derive(Default, Debug)]
pub struct ExportInfo {
    pub template_files_written: u32,
    pub template_files_failed: u32,
    pub static_files_copied: u32,
    pub static_files_failed: u32,
}

impl ExportInfo {
    pub fn template_write_success(&mut self) -> u32 {
        self.template_files_written += 1;
        self.template_files_written
    }

    pub fn template_write_failure(&mut self) -> u32 {
        self.template_files_failed += 1;
        self.template_files_failed
    }

    pub fn static_copy_success(&mut self) -> u32 {
        self.static_files_copied += 1;
        self.static_files_copied
    }

    pub fn static_copy_failure(&mut self) -> u32 {
        self.static_files_failed += 1;
        self.static_files_failed
    }
}

impl Blog {
    /// Create the pages and posts for the export.
    pub fn new(
        pages: Vec<String>,
        posts_dir: Option<String>,
        static_passthru: Vec<String>,
        layout_path: &PathBuf,
        export_target_dir: &str,
    ) -> Self {
        let pages = Self::page_templates(pages);

        let mut posts: Vec<MarkdownPage> = Vec::new();
        if let Some(posts_dir) = posts_dir {
            posts = Self::post_templates(posts_dir).unwrap_or(Vec::new());
        }

        Blog {
            pages,
            posts,
            static_passthru,
            layout_path: layout_path.to_owned(),
            export_target_dir: export_target_dir.to_owned(),
        }
    }

    /// Generate pages for all templates listed in `pages`. Pages with errors are skipped.
    fn _page_templates(files: Vec<String>) -> Vec<MarkdownPage> {
        files
            .iter()
            .filter_map(|filename| {
                let path = Path::new(filename);
                let ext = path.extension()?.to_str()?;
                match ext {
                    "md" => MarkdownPage::from_file_path(path).ok(),
                    "liquid" => MarkdownPage::from_file_path(path).ok(),
                    _ => None,
                }
            })
            .collect()
    }

    /// Generate pages for all templates listed in `pages`. Pages with errors are skipped.
    fn page_templates(files: Vec<String>) -> Vec<Box<dyn Page>> {
        let mut pages: Vec<Box<dyn Page>> = vec![];
        let options = MatchOptions {
            case_sensitive: true,
            require_literal_separator: true,
            require_literal_leading_dot: true,
        };

        // This is huge match is kind of a bummer. I tried using filter_map
        // and ran into issues with the types so I did this instead.
        files.iter().for_each(|fileglob| {
            for filename in glob_with(fileglob, options).expect("failed to list files") {
                let filename = filename.unwrap();
                let ext = filename.extension();
                match ext {
                    None => (),
                    Some(os_str) => match os_str.to_str() {
                        None => (),
                        Some("md") => match MarkdownPage::from_file_path(&filename) {
                            Err(_) => (),
                            Ok(page) => { pages.push(Box::new(page)); },
                        },
                        Some("liquid") => match LiquidPage::from_file_path(&filename) {
                            Err(_) => (),
                            Ok(page) => { pages.push(Box::new(page)); },
                        },
                        Some(ext) => warn!("Unrecognized file extension {}", ext),
                    },
                }
            }
        });
        pages
    }

    /// Generate pages for all .md files in `posts_dir`. Posts with errors are skipped.
    fn post_templates(posts_dir: String) -> Result<Vec<MarkdownPage>> {
        let mut pages: Vec<MarkdownPage> = Vec::new();
        let posts_path = Path::new(&posts_dir);
        for entry in fs::read_dir(posts_path)? {
            let entry = entry?;
            let path = entry.path();
            if let Ok(filename) = entry.file_name().into_string() {
                // skip hidden files
                if filename.starts_with('.') || filename.starts_with('_') {
                    info!("skipping {}", path.display());
                    continue;
                }
                // only process .md files
                if !path.is_dir() && filename.ends_with(".md") {
                    let input_path = posts_path.join(&filename);
                    match MarkdownPage::from_file_path(&input_path) {
                        Err(msg) => error!("failed to read post `{}`: {:?}", filename, msg),
                        Ok(page) => pages.push(page),
                    }
                }
            }
        }

        // sort descending, we want the newest post first
        pages.sort_by(|a, b| b.cmp(a));

        Ok(pages)
    }

    ///  Generate the `posts` in a format exportable to Liquid.
    ///  This is used to list posts in the Liquid template.
    fn as_collection(&self) -> Vec<CollectionPost> {
        self.posts
            .clone()
            .into_iter()
            .filter(|x| !x.is_draft())
            .map(CollectionPost::from)
            .collect()
    }

    /// Export this blog to a web site in the configured target directory.
    pub fn export(&self) -> io::Result<ExportInfo> {
        let mut track: ExportInfo = Default::default();

        for path in self.static_passthru.iter() {
            let source = Path::new(path);

            let mut target = PathBuf::new();
            target.push(self.export_target_dir.clone());
            target.push(source);

            if source.is_dir() {
                match copy_dir(source, target) {
                    Ok(_) => {
                        track.static_copy_success();
                        info!("copied dir {}", source.display())
                    }
                    Err(m) => {
                        track.static_copy_failure();
                        error!("failed to copy `{}`: {:?}", source.display(), m)
                    }
                }
            } else {
                match fs::copy(source, target) {
                    Ok(_) => {
                        track.static_copy_success();
                        info!("copied file {}", source.display())
                    }
                    Err(m) => {
                        track.static_copy_failure();
                        error!("failed to copy `{}`: {:?}", source.display(), m)
                    }
                }
            }
        }
        for post in self.posts.iter() {
            match post.export(
                &self.export_target_dir,
                &self.layout_path,
                self.as_collection(),
            ) {
                Ok(_) => {
                    track.template_write_success();
                    info!("exported {}", post.file_name())
                }
                Err(m) => {
                    track.template_write_failure();
                    error!("failed to export `{}`: {:?}", post.file_name(), m)
                }
            }
        }
        for page in self.pages.iter() {
            match page.export(
                &self.export_target_dir,
                &self.layout_path,
                self.as_collection(),
            ) {
                Ok(_) => {
                    track.template_write_success();
                    info!("exported {}", page.file_name())
                }
                Err(m) => {
                    track.template_write_failure();
                    error!("failed to export `{}`: {:?}", page.file_name(), m)
                }
            }
        }
        Ok(track)
    }
}
