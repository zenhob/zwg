# Example

This is a sample index for `zwg`.

## Latest Posts

{% for post in posts limit:1 %}
  [{{ post.info.title }}]({{ post.url }})
  *{{ post.date | date: "%A %B %e, %Y" }}*
{% endfor %}
